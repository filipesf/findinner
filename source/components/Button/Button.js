import React from 'react';
import './button.scss';

const Button = props => {
  return (
    <button className={props.class}>{props.text}</button>
  );
}

export default Button;
