import React, { Component } from 'react';
import restaurants from '../../data/restaurants.json';
import Card from './Card';

export default class CardContainer extends Component {
  constructor() {
    super();
    this.state = {
      restaurants: []
    };
  }

  componentWillMount() {
    this.setState({
      restaurants: restaurants
    });
  }

  render() {
    const results = this.state.restaurants;
    let restaurants = results.map((restaurant, index) =>
      <Card
        key={index}
        restaurant={restaurant}
      />
    );

    return restaurants;
  }
}
