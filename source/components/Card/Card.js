import React from 'react';
import './card.scss';

import Criteria from '../Criteria/Criteria';
import Rating   from '../Rating/Rating';
import Button   from '../Button/Button';

const Card = props => {
  const r = props.restaurant;

  return (
    <div className="card">
      <figure
        className="card__image"
        style={{backgroundImage: 'url(' + r.image +')'}}
      ></figure>
      <section className="card__content">
        <header className="card__header">
          <span className="card__category">{r.category}</span>
          <h1 className="card__title">{r.name}</h1>
          <address className="card__address">{r.address}</address>
        </header>
        <Criteria
          price={r.criteria.price}
          location={r.criteria.location}
          cuisine={r.criteria.cuisine}
          wineCard={r.criteria.wineCard}
        />
        <Rating />
        <Button
          text="Book a table"
          class="button button--primary"
        />
        <Button
          text="Share"
          class="button"
        />
      </section>
    </div>
  );
}

export default Card;
