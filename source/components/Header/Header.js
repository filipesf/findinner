import React, { Component } from 'react';
import './header.scss';

import Search from '../Search/Search';

class Header extends Component {
  render() {
    return (
      <header className="header">
        <h1 className="header__title"><span>Find</span>inner</h1>
        <h2 className="header__headline">Find your dinner</h2>
        <Search />
      </header>
    );
  }
}

export default Header;
