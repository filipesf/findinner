import React, { Component } from 'react';
import './footer.scss';

class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <span className="footer__copy">© 2017 ITONICS | All Rights Reserved | Imprint | Privacy Policies</span>
      </footer>
    );
  }
}

export default Footer;
