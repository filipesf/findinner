import React, { Component } from 'react';
import './rating.scss';

class Rating extends Component {
  render() {
    return (
      <ul className="rating">
        <p className="rating__usr">My Rating: <strong>star</strong></p>
        <p className="rating__avg">Average Rating: <strong>avg</strong></p>

        <li className="rating__star"></li>
        <li className="rating__star"></li>
        <li className="rating__star"></li>
        <li className="rating__star"></li>
        <li className="rating__star is-inactive"></li>
      </ul>
    );
  }
}

export default Rating;
