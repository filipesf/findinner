import React, { Component } from 'react';
import './app.scss';

import Header        from '../Header/Header';
import CardContainer from '../Card/CardContainer';
import Footer        from '../Footer/Footer';

class App extends Component {
  render() {
    return (
      <div id="App" className="App">
        <Header />
        <div className="l-wrapper">
          <CardContainer />
        </div>
        <Footer />
      </div>
    );
  }
}

export default App;
