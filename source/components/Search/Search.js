import React, { Component } from 'react';
import './search.scss';

class Search extends Component {
  render() {
    return (
      <form className="search">
        <input className="search__input" placeholder="Filter your search results"/>
      </form>
    );
  }
}

export default Search;
