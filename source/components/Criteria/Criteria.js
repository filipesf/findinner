import React from 'react';
import './criteria.scss';

const Criteria = props => {
  return (
    <ul className="criterias">
      <li className="criterias__item">
        <div className="criterias__value">{props.price}%</div>
        <div className="criterias__label">Price</div>
      </li>
      <li className="criterias__item">
        <div className="criterias__value">{props.location}%</div>
        <div className="criterias__label">Location</div>
      </li>
      <li className="criterias__item">
        <div className="criterias__value">{props.cuisine}%</div>
        <div className="criterias__label">Cuisine</div>
      </li>
      <li className="criterias__item">
        <div className="criterias__value">{props.wineCard}%</div>
        <div className="criterias__label">Wine Card</div>
      </li>
    </ul>
  );
}

export default Criteria;
